package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.settings.SettingsContract
import bg.devlabs.sparrow.ui.settings.SettingsFragment
import bg.devlabs.sparrow.ui.settings.SettingsPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class SettingsModule {
    @Binds
    abstract fun provideView(fragment: SettingsFragment): SettingsContract.View

    @Binds
    abstract fun providePresenter(presenter: SettingsPresenter): SettingsContract.Presenter
}