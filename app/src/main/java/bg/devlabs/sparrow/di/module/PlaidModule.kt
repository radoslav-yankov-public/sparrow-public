package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.plaid.PlaidContract
import bg.devlabs.sparrow.ui.plaid.PlaidFragment
import bg.devlabs.sparrow.ui.plaid.PlaidPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class PlaidModule {
    @Binds
    abstract fun provideView(fragment: PlaidFragment): PlaidContract.View

    @Binds
    abstract fun providePresenter(presenter: PlaidPresenter): PlaidContract.Presenter
}