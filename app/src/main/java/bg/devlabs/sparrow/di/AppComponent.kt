package bg.devlabs.sparrow.di

import android.app.Application

import javax.inject.Singleton

import bg.devlabs.sparrow.Sparrow
import bg.devlabs.sparrow.di.module.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */
@Singleton
@Component(modules = [(AndroidInjectionModule::class), (AppModule::class), (ActivityBindingModule::class)])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: Sparrow)
}