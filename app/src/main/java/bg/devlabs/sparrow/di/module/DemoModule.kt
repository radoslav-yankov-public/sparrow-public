package bg.devlabs.sparrow.di.module

import bg.devlabs.sparrow.ui.demo.DemoContract
import bg.devlabs.sparrow.ui.demo.DemoFragment
import bg.devlabs.sparrow.ui.demo.DemoPresenter
import dagger.Binds
import dagger.Module


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
@Module
abstract class DemoModule {
    @Binds
    abstract fun provideView(fragment: DemoFragment): DemoContract.View

    @Binds
    abstract fun providePresenter(presenter: DemoPresenter): DemoContract.Presenter
}