package bg.devlabs.sparrow.utils

import android.app.Activity
import android.content.Context
import android.graphics.Point
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.home.HomeActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */




fun TextView.makeLinks(links: Array<String>, clickableSpans: Array<ClickableSpan>) {
    val spannableString = SpannableString(text)
    for (i in links.indices) {
        val clickableSpan = clickableSpans[i]
        val link = links[i]
        val startIndex = text.toString().indexOf(link)
        val endIndex = startIndex + link.length
        spannableString.setSpan(clickableSpan, startIndex, endIndex,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
    }
    movementMethod = LinkMovementMethod.getInstance()
    setText(spannableString, TextView.BufferType.SPANNABLE)
}

fun View.changeHeight(value: Int) {
    layoutParams.height = value
    requestLayout()
}

fun Activity.setActionBarTitle(text: String) {
    ((this as HomeActivity).supportActionBar?.customView?.findViewById(R.id.title) as TextView).alpha = 0f
    delay(100) {
        (supportActionBar?.customView?.findViewById(R.id.title) as TextView).text = text
        (supportActionBar?.customView?.findViewById(R.id.title) as TextView).animate().alpha(1f).duration = 200
    }
//    (this as HomeActivity).supportActionBar?.title = Html.fromHtml("<font color='#123749'>$text</font>")
}

fun getFormattedDate() = SimpleDateFormat("EEEE, MM/d").format(Calendar.getInstance().time)

fun Double?.text() = this.toString()

fun TextView.setFont(name: String) {
    val typeface = Typeface.createFromAsset(this.context.applicationContext.assets,
            String.format(Locale.US, "fonts/%s", name))
    this.typeface = typeface
}

fun Button.setFont(name: String) {
    val typeface = Typeface.createFromAsset(this.context.applicationContext.assets,
            String.format(Locale.US, "fonts/%s", name))
    this.typeface = typeface
}


fun Button.disable() {
    this.animate().alpha(0.0f)
    this.isEnabled = false
}

fun Button.enable() {
    this.animate().alpha(1.0f)
    this.isEnabled = true
}

fun ImageView.load(url: String) {
    Glide.with(this.context)
            .load(url)
            .apply(RequestOptions()
                    .centerCrop()
                    .circleCrop())
            .transition(DrawableTransitionOptions.withCrossFade())
            .into(this)

}

fun Fragment.showKeyboard() {
    val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun EditText.focus() {
    this.isFocusableInTouchMode = true
    this.requestFocus()
    this.setSelection(this.text.toString().length)
    val imm = this.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)

}

fun ImageView.load(id: Int) {
    val options = RequestOptions()
            .priority(Priority.HIGH)
            .centerCrop()
            .circleCrop()
    Glide.with(this.context)
            .load(id)
            .apply(options)
            .into(this)
}

fun ImageView.setTint(colorId: Int) {
    this.setColorFilter(ContextCompat.getColor(context, colorId), android.graphics.PorterDuff.Mode.SRC_ATOP)

}

fun View.getDrawable(id: Int): Drawable {
    return context.getDrawable(id)
}

fun View.onClick(func: (() -> Unit)?) {
    this.setOnClickListener {
        func?.let { it1 -> it1() }
    }
}

val handler = Handler()
fun delay(delay: Long, func: () -> Unit) {
    handler.postDelayed({
        try {
            func()
        } catch (e: Exception) {
            println(e.toString())
        }
    }, delay)
}

infix fun (() -> Any).every(delay: Long) {

    this()
    val handler = Handler()
    val runnable = object : Runnable {
        override fun run() {
            try {
                this@every()
            } catch (e: Exception) {
            }
            handler.postDelayed(this, delay)
        }
    }
    handler.postDelayed(runnable, delay)
}

fun Fragment.getScreenWidth(): Int {
    val size = Point()
    activity?.windowManager?.defaultDisplay?.getSize(size)
    return size.x
}

fun Fragment.getScreenHeight(): Int {
    val size = Point()
    activity?.windowManager?.defaultDisplay?.getSize(size)
    return size.y
}

fun Activity.getScreenWidth(): Int {
    val size = Point()
    windowManager.defaultDisplay.getSize(size)
    return size.x
}

fun Activity.getScreenHeight(): Int {
    val size = Point()
    windowManager.defaultDisplay.getSize(size)
    return size.y
}

fun Fragment.getNavbarHeight(): Int {
    val resources = context?.resources
    val resourceId = resources?.getIdentifier("navigation_bar_height", "dimen", "android")
    return if (resourceId != null) {
        if (resourceId > 0) {
            resources.getDimensionPixelSize(resourceId)
        } else {
            0
        }
    } else {
        0
    }
}




inline fun <T: Any, R> T?.transform(block: (T) -> R) : R? {
    return if (this != null) block(this) else null
}


fun Fragment.hideKeyboard() {
    val inputMethodManager: InputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
}