package bg.devlabs.sparrow.data.network.model.userscriptsparam

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserScriptsParam_ {

    @SerializedName("actions")
    @Expose
    var actions: Actions? = null

}
