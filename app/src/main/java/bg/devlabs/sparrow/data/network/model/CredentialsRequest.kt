package bg.devlabs.sparrow.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by Radoslav Yankov on 02.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class CredentialsRequest(email: String = "", password: String = "", status: String = "") {
    @SerializedName("user")
    @Expose
    var user = if ( status == "") UserRequest(email = email, password = password) else UserRequest(status = status)
}