package bg.devlabs.sparrow.data.network.model.userdata

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserData_ {

    @SerializedName("values")
    @Expose
    var values: List<Value>? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("impact")
    @Expose
    var impact: List<Impact>? = null

}
