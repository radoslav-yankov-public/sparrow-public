package bg.devlabs.sparrow.data.network

import bg.devlabs.sparrow.utils.CLIENT_ID
import bg.devlabs.sparrow.data.network.model.CauseRequest
import bg.devlabs.sparrow.data.network.model.CredentialsRequest
import bg.devlabs.sparrow.data.network.model.UpdateUserRequest
import bg.devlabs.sparrow.data.network.model.bank.BankInfoRequest
import bg.devlabs.sparrow.data.network.model.userscriptsparam.Plaid
import bg.devlabs.sparrow.data.network.model.userscriptsparam.UserScriptsParamResponse
import bg.devlabs.sparrow.ui.plaid.Bank
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject
import javax.inject.Qualifier

/**
 * Created by Radoslav Yankov on 31.01.18
 * radoslav@devlabs.bg
 */

class AppNetworkHelper @Inject
constructor(retrofit: Retrofit, @ApiKey private val key: String) : NetworkHelper {


    private var service: ApiService = retrofit.create(ApiService::class.java)

    override fun createUser(email: String, password: String) = service.createUser(CredentialsRequest(email, password), CLIENT_ID)
    override fun login(email: String, password: String) = service.login(CredentialsRequest(email, password), CLIENT_ID)
    override fun getUser(userId: String, xAuth: String) = service.getUser(userId, CLIENT_ID, xAuth)
    override fun getUserData(userId: String, xAuth: String) = service.getUserData(userId, CLIENT_ID, xAuth)
    override fun updateUser(userId: String, xAuth: String, updatedData: Any): Observable<Response<Unit>> {
        return when (updatedData) {
            is String -> service.updateUser(userId, UpdateUserRequest(updatedData), CLIENT_ID, xAuth)
            is MutableList<*> -> {
                val newList = mutableListOf<CauseRequest>()
                updatedData.forEachIndexed { index, any ->
                    newList.add(CauseRequest(when (index) {
                        0 -> {
                            "Health & Disease"
                        }
                        1 -> {
                            "Poverty Reduction"
                        }
                        2 -> {
                            "Education"
                        }
                        3 -> {
                            "Environment & Future"
                        }
                        4 -> {
                            "Food, Water, & Shelter"
                        }
                        5 -> {
                            "Animals "
                        }
                        else -> {
                            "Value $index"
                        }
                    }, any as String))
                }
                service.updateUser(userId, UpdateUserRequest(causes = newList), CLIENT_ID, xAuth)
            }
            else -> service.updateUser(userId, UpdateUserRequest(updatedData.toString()), CLIENT_ID, xAuth)
        }
    }

    override fun getCoreValues(xAuth: String) = service.getCoreValues(CLIENT_ID, xAuth)
    override fun updateBankInfo(userId: String, xAuth: String, bank: Bank, bank2: Bank): Observable<retrofit2.Response<Unit>>  {
        getUserScriptsParam(userId, xAuth)
                .subscribe({ it: Result<UserScriptsParamResponse>? ->
                    if (it?.response()?.isSuccessful == true) {
                        it.response()?.body()?.userScriptsParam?.userScriptsParam?.actions?.plaid?.map { it: Plaid ->
                            if ((it.publicToken == bank.publicToken || it.publicToken == bank2.publicToken) && bank.id != null && bank2.id != null){
                                return@subscribe
                            }
                        }
                    }

                }, {
                })

        return service.updateBankInfo(userId,
                BankInfoRequest(bank, if (bank2.publicToken == bank.publicToken) null else bank2)
                , CLIENT_ID, xAuth)
    }

    override fun readyValue(eventId: String, xAuth: String) = service.readyValue(eventId, CLIENT_ID, xAuth)
    override fun unreadyValue(eventId: String, xAuth: String) = service.unreadyValue(eventId, CLIENT_ID, xAuth)
    override fun logout(xAuth: String) = service.logout(CLIENT_ID, xAuth)
    override fun delete(userId: String, xAuth: String) = service.delete(userId, CLIENT_ID, xAuth)
    override fun getUserScriptsParam(userId: String, xAuth: String) = service.getUserScriptsParam(userId, CLIENT_ID, xAuth)
    override fun setStatus(userId: String, xAuth: String, status: String) = service.setStatus(userId, xAuth, CredentialsRequest(status = status), CLIENT_ID)
}

@Qualifier
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ApiKey


