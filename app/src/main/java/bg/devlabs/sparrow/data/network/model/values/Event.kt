package bg.devlabs.sparrow.data.network.model.values

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Event {

    @SerializedName("eventId")
    @Expose
    var eventId: String? = null
    @SerializedName("status")
    @Expose
    var status: Status? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("description")
    @Expose
    var description: List<String>? = null

}
