package bg.devlabs.sparrow.data.auth

import android.content.Intent
import com.facebook.CallbackManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import io.reactivex.Single


/**
 * Created by Slavi Petrov on 20.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
interface AuthHelper {
    val googleSignInClient: GoogleSignInClient
    val facebookCallbackManager: CallbackManager
    fun isGoogleAccountSignedIn(): Boolean
    fun onGoogleSignInResult(data: Intent?): Single<GoogleSignInAccount>
    fun onFacebookSignInResult(requestCode: Int, resultCode: Int, data: Intent?)
    fun registerFacebookLoginCallback(): Single<LoginResult>?
}