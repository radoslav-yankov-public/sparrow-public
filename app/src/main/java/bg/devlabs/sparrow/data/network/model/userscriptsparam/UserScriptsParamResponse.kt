package bg.devlabs.sparrow.data.network.model.userscriptsparam

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserScriptsParamResponse {

    @SerializedName("userScriptsParam")
    @Expose
    var userScriptsParam: UserScriptsParam? = null

}
