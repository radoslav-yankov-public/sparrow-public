package bg.devlabs.sparrow.data.network.model.userscriptsparam

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Plaid {

    @SerializedName("account_id")
    @Expose
    var accountId: String? = null
    @SerializedName("account_type")
    @Expose
    var accountType: String? = null
    @SerializedName("accounts")
    @Expose
    var accounts: String? = null
    @SerializedName("institution_name")
    @Expose
    var institutionName: String? = null
    @SerializedName("institution_id")
    @Expose
    var institutionId: String? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("public_token")
    @Expose
    var publicToken: String? = null
    @SerializedName("account_name")
    @Expose
    var accountName: String? = null
    @SerializedName("_id")
    @Expose
    var id: String? = null

}
