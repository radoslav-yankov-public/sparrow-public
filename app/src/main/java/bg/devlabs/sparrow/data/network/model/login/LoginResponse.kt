package bg.devlabs.sparrow.data.network.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginResponse {

    @SerializedName("user")
    @Expose
    var user: User? = null

}
