package bg.devlabs.sparrow.data.network.model.userRead

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserRead {

    @SerializedName("user")
    @Expose
    var user: User? = null

}
