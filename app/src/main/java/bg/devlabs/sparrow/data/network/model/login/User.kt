package bg.devlabs.sparrow.data.network.model.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("userId")
    @Expose
    var userId: String? = null

    @SerializedName("status")
    @Expose
    var status: String? = null

}
