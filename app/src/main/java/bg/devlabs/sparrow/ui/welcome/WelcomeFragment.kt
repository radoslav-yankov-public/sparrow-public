package bg.devlabs.sparrow.ui

import android.content.Intent
import android.os.Bundle
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.main.MainFragment
import bg.devlabs.sparrow.ui.signin.SignInFragment
import bg.devlabs.sparrow.ui.signup.SignUpFragment
import bg.devlabs.sparrow.ui.welcome.WelcomeContract
import bg.devlabs.sparrow.utils.makeLinks
import bg.devlabs.sparrow.utils.setActionBarTitle
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import kotlinx.android.synthetic.main.fragment_welcome.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class WelcomeFragment : InjectionBaseFragment(), WelcomeContract.View {

    @Inject
    lateinit var presenter: WelcomeContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupFacebookSignInButton()
        setupGoogleSignInButton()
        setupSignInSignUpTextView()
        setupTermsConditionsTextView()
        activity?.setActionBarTitle(getString(R.string.title_welcome))
    }

    private fun setupFacebookSignInButton() {
        login_facebook_button.setOnClickListener {
            presenter.onFacebookSignInButtonClicked()
            LoginManager.getInstance().logInWithReadPermissions(this, mutableListOf("email"))
        }
    }

    private fun setupGoogleSignInButton() {
        login_google_button.setOnClickListener {
            presenter.onGoogleSignInButtonClicked()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupTermsConditionsTextView() {
        val termsConditionsClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View?) {
                Toast.makeText(activity, "terms and conditions", Toast.LENGTH_LONG).show()
            }
        }
        val termsConditionsText = getString(R.string.text_terms_conditions_span)
        termsConditionsTextView.makeLinks(arrayOf(termsConditionsText),
                arrayOf(termsConditionsClickableSpan))
    }

    private fun setupSignInSignUpTextView() {
        val logInClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View?) {
                showFragment(SignInFragment(), R.string.sign_in_fragment)
            }
        }
        val signUpClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View?) {
                showFragment(SignUpFragment(), R.string.sign_up_fragment)
            }
        }
        val logInText = getString(R.string.log_in)
        val signUpText = getString(R.string.sign_up)
        signUpSignInTextView.makeLinks(arrayOf(logInText, signUpText),
                arrayOf(logInClickableSpan, signUpClickableSpan))
    }

    override fun onGoogleLoginSuccess(account: GoogleSignInAccount?) {
        showFragment(MainFragment(), R.string.main_fragment)
    }
}