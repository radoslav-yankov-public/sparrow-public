package bg.devlabs.sparrow.ui.causes

import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.ui.base.BaseContract
import bg.devlabs.sparrow.ui.plaid.Bank

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class
CausesContract {
    interface View : BaseContract.View {
        fun onSuccess()
        fun setupCauseViews(response: UserDataResponse?)
    }

    interface Presenter : BaseContract.Presenter {
        fun updateBankInfo(bank: Bank, bank2: Bank)
        fun updateProfileInfo(causesList: MutableList<String>)
        fun saveOnboarding()
    }
}
