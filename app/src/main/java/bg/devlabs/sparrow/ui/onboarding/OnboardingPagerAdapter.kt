package bg.devlabs.sparrow.ui.onboarding

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import bg.devlabs.sparrow.R


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class OnboardingPagerAdapter(fm: FragmentManager?) : FragmentStatePagerAdapter(fm) {
    private val onboardingItems = arrayOf(
            OnboardingItem(R.drawable.ic_sparrow, R.string.onboarding_desc_item_1),
            OnboardingItem(R.drawable.ic_list, R.string.onboarding_desc_item_2),
            OnboardingItem(R.drawable.ic_date, R.string.onboarding_desc_item_3),
            OnboardingItem(R.drawable.ic_globe, R.string.onboarding_desc_item_4),
            OnboardingItem(R.drawable.ic_stopwatch, R.string.onboarding_desc_item_5))

    override fun getItem(position: Int): Fragment {
        val fragment = OnboardingItemFragment()
        val (imageResId, descriptionResId) = onboardingItems[position]
        fragment.imageResId = imageResId
        fragment.descriptionResId = descriptionResId
        return fragment
    }

    override fun getCount(): Int {
        return 5
    }
}