package bg.devlabs.sparrow.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.base.BaseFragment
import bg.devlabs.sparrow.ui.plaid.PlaidFragment
import kotlinx.android.synthetic.main.fragment_fund_account.*


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class FundAccountFragment : BaseFragment() {
    var publicToken = ""
    private var accountId = ""

    private fun setData(publicToken: String, accountId: String) {
        this.publicToken = publicToken
        this.accountId = accountId
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_fund_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        continueButton.setOnClickListener {
            showFragment(PlaidFragment().apply {
                setData(this@FundAccountFragment.publicToken,
                        this@FundAccountFragment.accountId)
            }, R.string.plaid_fragment)
        }
    }
}