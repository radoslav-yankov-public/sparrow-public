package bg.devlabs.sparrow.ui.profile

import bg.devlabs.sparrow.ui.base.BasePresenter
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class ProfilePresenter @Inject constructor(view: ProfileContract.View)
    : BasePresenter<ProfileContract.View>(view), ProfileContract.Presenter {

    override fun onViewBind() {
        compositeDisposable.add(dataManager.getUserData(dataManager.userId, dataManager.xAuth)
                .subscribe({
                    if (!it.isError) {
                        view?.onDataLoaded(ProfileData(it?.response()?.body()?.userData?.userData?.name
                                ?: "User", "https://x1.xingassets.com/assets/frontend_minified/img/users/nobody_m.original.jpg"))
                    } else {
                        view?.showErrorToast()
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }

    override fun onNameClick(name: String) {
        compositeDisposable.add(dataManager.updateUser(dataManager.userId, dataManager.xAuth, name)
                .subscribe({
                    view?.showToast("Name Changed!")
                }, {
                    view?.showErrorToast()
                })
        )
    }
}