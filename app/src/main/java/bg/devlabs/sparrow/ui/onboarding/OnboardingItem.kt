package bg.devlabs.sparrow.ui.onboarding


/**
 * Created by Slavi Petrov on 15.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
data class OnboardingItem(val imageResId: Int, val descriptionResId: Int)