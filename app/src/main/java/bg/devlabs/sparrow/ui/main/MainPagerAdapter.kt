package bg.devlabs.sparrow.ui.main

import android.arch.lifecycle.MutableLiveData
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import bg.devlabs.sparrow.utils.ContextAction
import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.impact.ImpactFragment
import bg.devlabs.sparrow.ui.corevalues.CoreValuesFragment


/**
 * Created by Slavi Petrov on 08.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class MainPagerAdapter(fragmentManager: FragmentManager)
    : FragmentStatePagerAdapter(fragmentManager) {

    lateinit var data: MutableLiveData<List<Event>>
    private val fragmentCount = 2

    override fun getItem(position: Int): Fragment {
        val fragment: Fragment
        if (position == 0) {
            fragment = CoreValuesFragment()
//            fragment.showGoInsideButton = false
            fragment.contextAction = ContextAction.MAIN
        } else {
            fragment = ImpactFragment()
        }
        return fragment
//        return DiscoverActiveFragment().apply {
//            data = this@CoreValuesPagerAdapter.data?.apply {
//                events = events?.filter { it: Event ->
//                    if (position == 0) {
//                        !(it.status?.ready ?: true)
//                    } else {
//                        it.status?.ready ?: false
//                    }
//                }
//            }
//        }
    }

    override fun getCount(): Int {
        return fragmentCount
    }


//    override fun getPageTitle(position: Int): CharSequence? {
//        return when (position) {
//            0 -> "Discover"
//            1 -> "Active"
//            else -> {
//                ""
//            }
//        }
//    }
}