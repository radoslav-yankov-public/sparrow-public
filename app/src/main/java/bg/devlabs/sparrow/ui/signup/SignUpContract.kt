package bg.devlabs.sparrow.ui.signup

import bg.devlabs.sparrow.ui.base.BaseContract


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
interface SignUpContract {
    interface View : BaseContract.View {
        fun onSignUpSuccess()
        fun onSignUpFail()
    }

    interface Presenter : BaseContract.Presenter {
        fun onSignUpButtonClicked(email: String, password: String, name: String)
    }
}