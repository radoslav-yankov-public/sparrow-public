package bg.devlabs.sparrow.ui.corevalues

import android.arch.lifecycle.MutableLiveData
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.discoveractive.DiscoverActiveFragment


/**
 * Created by Slavi Petrov on 06.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class CoreValuesPagerAdapter(private var coreValuesViewPager: ViewPager, fragmentManager: FragmentManager)
    : FragmentStatePagerAdapter(fragmentManager) {

    lateinit var data: MutableLiveData<List<Event>>
    private val fragmentCount = 2

    override fun getItem(position: Int): Fragment {
        val fragment = DiscoverActiveFragment()
        fragment.pager = coreValuesViewPager
        fragment.showReady = position != 0
        fragment.data = data
        return fragment
//        return DiscoverActiveFragment().apply {
//            data = this@CoreValuesPagerAdapter.data?.apply {
//                events = events?.filter { it: Event ->
//                    if (position == 0) {
//                        !(it.status?.ready ?: true)
//                    } else {
//                        it.status?.ready ?: false
//                    }
//                }
//            }
//        }

    }

    override fun getCount(): Int {
        return fragmentCount
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Discover"
            1 -> "Active"
            else -> {
                null
            }
        }
    }
}