package bg.devlabs.sparrow.ui.home


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
enum class ToolbarButtonType {
    PROFILE, SETTINGS
}