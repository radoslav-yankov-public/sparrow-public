package bg.devlabs.sparrow.ui.base

import android.content.Context
import dagger.android.support.AndroidSupportInjection

/**
 * Created by Radoslav Yankov on 31.1.18.
 */

abstract class InjectionBaseFragment : BaseFragment() {
    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
