package bg.devlabs.sparrow.ui.signin

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.KeyboardHideFragment
import bg.devlabs.sparrow.ui.main.MainFragment
import bg.devlabs.sparrow.utils.disable
import bg.devlabs.sparrow.utils.enable
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_sign_in.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SignInFragment : KeyboardHideFragment(), SignInContract.View {
    @Inject
    lateinit var presenter: SignInContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.setActionBarTitle(getString(R.string.app_name))
        sign_in_button.setOnClickListener {
            sign_in_button.disable()
            presenter.onSignInButtonClicked(emailTextInputLayout.editText?.text.toString(),
                    passwordTextInputLayout.editText?.text.toString())
        }
        forgotPasswordTextView.setOnClickListener {
            presenter.onForgotPasswordClicked()
        }
        setupPasswordEditText()
    }

    private fun setupPasswordEditText() {
        password_edit_text.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                sign_in_button.callOnClick()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    override fun onSignInSuccess() {
        sign_in_button?.enable()
        showFragment(MainFragment(), R.string.main_fragment)
//        showToast("Logged in")
//        showFragment(FundAccountFragment(), R.string.fund_account_fragment)
    }

    override fun onSignInFail() {
        sign_in_button?.enable()
        showErrorToast()


//        showToast("Logged in")
//        showFragment(FundAccountFragment(), R.string.fund_account_fragment)
    }
}