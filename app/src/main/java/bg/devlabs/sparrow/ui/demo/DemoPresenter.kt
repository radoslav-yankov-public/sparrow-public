package bg.devlabs.sparrow.ui.demo

import bg.devlabs.sparrow.ui.base.BasePresenter
import javax.inject.Inject

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class DemoPresenter @Inject
constructor(view: DemoContract.View) : BasePresenter<DemoContract.View>(view),
        DemoContract.Presenter {

    override fun updateBankInfo(publicToken: String, accountId: String) {
//        dataManager.updateBankInfo(
//                dataManager.userId,
//                dataManager.xAuth,
//                publicToken, accountId, "")
//                .subscribe({
//                    if (it.code().toString().startsWith("2")) {
//                        view.showToast("Success")
//                    }
//                }, {
//                    view.showErrorToast()
//                })
    }
}