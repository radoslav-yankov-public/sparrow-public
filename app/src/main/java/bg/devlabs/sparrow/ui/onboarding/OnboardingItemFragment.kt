package bg.devlabs.sparrow.ui.onboarding

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import kotlinx.android.synthetic.main.onboarding_item.*


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class OnboardingItemFragment : Fragment() {
    var imageResId: Int = 0
    var descriptionResId: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.onboarding_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        onboarding_image_view.setImageDrawable(ContextCompat.getDrawable(view.context, imageResId))
        onboarding_desc_text_view.setText(descriptionResId)
    }
}