package bg.devlabs.sparrow.ui.corevalues

import android.arch.lifecycle.MutableLiveData
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.main.MainFragment
import bg.devlabs.sparrow.utils.ContextAction
import bg.devlabs.sparrow.utils.onClick
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_core_values.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 06.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class CoreValuesFragment : InjectionBaseFragment(), CoreValuesContract.View {
    @Inject
    lateinit var presenter: CoreValuesContract.Presenter
    lateinit var adapter: CoreValuesPagerAdapter
//    var showGoInsideButton = true

    var contextAction: ContextAction = ContextAction.SIGNUP

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        //        presenter.onViewBind()
        return inflater.inflate(R.layout.fragment_core_values, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        activity?.setActionBarTitle(getString(R.string.core_values))

        setupFragmentContext()
    }

    override fun onResume() {
        presenter.onListLoad()
        super.onResume()
    }

    private fun setupFragmentContext() {
        when (contextAction) {
            ContextAction.SIGNUP -> {
                goInsideButton.visibility = View.VISIBLE
                activity?.setActionBarTitle(getString(R.string.title_pick_your_rules))

            }
            ContextAction.MAIN -> {
                goInsideButton.visibility = View.GONE

            }
            ContextAction.SETTINGS -> {
                goInsideButton.visibility = View.VISIBLE
                goInsideButton.text = getString(R.string.text_done)
                activity?.setActionBarTitle(getString(R.string.title_core_values))
            }

        }
    }

    override fun updateValuesList(data: List<Event>?) {
        adapter = CoreValuesPagerAdapter(coreValuesViewPager, childFragmentManager)
        val events: MutableLiveData<List<Event>> = MutableLiveData()
        events.postValue(data)
        adapter.data = events
        coreValuesViewPager.adapter = adapter
//        tabLayout.background = resources.getDrawable(R.color.white)
        tabLayout.setupWithViewPager(coreValuesViewPager)
        tabLayout.elevation = 5f
        goInsideButton.onClick {
            when (contextAction) {
                ContextAction.SIGNUP -> {
                    showFragment(MainFragment(), R.string.main_fragment)
                    presenter.saveOnboarding()

                }
                ContextAction.MAIN -> {
                    showFragment(MainFragment(), R.string.main_fragment)

                }
                ContextAction.SETTINGS -> {
                    activity?.onBackPressed()
                }

            }
        }
    }

//    var oldData = listOf<Event>()
//
//    override fun updateValuesList(data: List<Event>?) {
//        if (data?.filter { it.status?.ready == true } != oldData?.filter { it.status?.ready == true }) {
//            updateData(data)
//        }
//        oldData = data ?: listOf()
//        updateUi()
//    }
//
//    private fun updateData(data: List<Event>?) {
//        adapter = CoreValuesPagerAdapter(coreValuesViewPager, childFragmentManager)
//        val events: MutableLiveData<List<Event>> = MutableLiveData()
//        events.postValue(data)
//        adapter.data = events
//        coreValuesViewPager.adapter = adapter
////        tabLayout.background = resources.getDrawable(R.color.white)
//        tabLayout.setupWithViewPager(coreValuesViewPager)
//    }
//
//    private fun updateUi() {
//        tabLayout.elevation = 5f
//        goInsideButton.onClick {
//            when (contextAction) {
//                ContextAction.SIGNUP -> {
//                    showFragment(MainFragment(), R.string.main_fragment)
//
//                }
//                ContextAction.MAIN -> {
//                    showFragment(MainFragment(), R.string.main_fragment)
//
//                }
//                ContextAction.SETTINGS -> {
//                    activity?.onBackPressed()
//                }
//
//            }
//        }
//    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }
}