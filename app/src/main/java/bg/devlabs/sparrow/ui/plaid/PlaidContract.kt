package bg.devlabs.sparrow.ui.plaid

import bg.devlabs.sparrow.ui.base.BaseContract

/**
 * Created by Radoslav Yankov on 31.01.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
class PlaidContract {
    interface View : BaseContract.View{
        fun onSuccess(bank1: Bank, bank2: Bank)
    }

    interface Presenter : BaseContract.Presenter {
        fun updateBankInfo(bank1: Bank, bank2: Bank)
    }
}
