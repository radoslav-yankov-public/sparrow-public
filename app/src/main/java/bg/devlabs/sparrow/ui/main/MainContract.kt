package bg.devlabs.sparrow.ui.main

import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.base.BaseContract


/**
 * Created by Slavi Petrov on 08.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class MainContract {
    interface View : BaseContract.View {
        fun updateValuesList(data: List<Event>?)
    }

    interface Presenter : BaseContract.Presenter {
        fun onListLoad()
    }
}