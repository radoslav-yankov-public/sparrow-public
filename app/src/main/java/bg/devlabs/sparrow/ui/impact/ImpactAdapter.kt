package bg.devlabs.sparrow.ui.impact


/**
 * Created by Radoslav Yankov on 07.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.userdata.Impact
import bg.devlabs.sparrow.data.network.model.userdata.UserDataResponse
import bg.devlabs.sparrow.utils.load
import bg.devlabs.sparrow.utils.setTint
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_impact.*


/**
 * Created by Radoslav Yankov on 29.11.2017
 * Dev Labs
 * radoslavyankov@gmail.com
 */

class ImpactAdapter(var presenter: ImpactContract.Presenter, private var data: UserDataResponse?) :
        RecyclerView.Adapter<ImpactAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val containerView = inflater.inflate(R.layout.item_impact, parent, false)
        return ViewHolder(presenter, containerView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setup(data?.userData?.userData?.impact?.get(position), position)
    }

    override fun getItemCount(): Int {
        return data?.userData?.userData?.impact?.size ?: 0
    }


    class ViewHolder(val presenter: ImpactContract.Presenter, override val containerView: View?)
        : RecyclerView.ViewHolder(containerView), LayoutContainer {

        fun setup(item: Impact?, currentPosition: Int = 0) {
            title.text = item?.name
            value.text = item?.value ?: ""
            imageView.setTint(R.color.md_grey_600)
            imageView.load(
                    when (item?.name) {
                        "Bednets provided" -> {
                            R.drawable.bednets_provided
                        }
                        "Children dewormed" -> {
                            R.drawable.children_dewormed
                        }
                        "People supported" -> {
                            R.drawable.people_supported
                        }
                        "Incomes increased" -> {
                            R.drawable.incomes_increased
                        }
                        "Children healthy for school" -> {
                            R.drawable.children_healthy_for_school
                        }
                        "Pounds of Carbon Offsets" -> {
                            R.drawable.pounds_of_carbon_offsets
                        }
                        "Micronutrients for meals" -> {
                            R.drawable.micronutrients_for_meals
                        }
                        "Animals saved from suffering" -> {
                            R.drawable.animals_saved_from_suffering
                        }
                        else -> {
                            R.drawable.ic_sparrow
                        }


                    })
        }

    }

}