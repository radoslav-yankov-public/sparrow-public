package bg.devlabs.sparrow.ui.discoveractive

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.data.network.model.values.Event
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import jp.wasabeef.recyclerview.animators.SlideInLeftAnimator
import jp.wasabeef.recyclerview.animators.SlideInRightAnimator
import kotlinx.android.synthetic.main.fragment_discover_active.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 06.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class DiscoverActiveFragment : InjectionBaseFragment(), DiscoverActiveContract.View {

    @Inject
    lateinit var presenter: DiscoverActiveContract.Presenter
    lateinit var data: MutableLiveData<List<Event>>
    lateinit var adapter: DiscoverActiveAdapter
    lateinit var pager: ViewPager
    var showReady = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter.onViewBind()
        return inflater.inflate(R.layout.fragment_discover_active, container, false)
    }

    override fun onCheckChangeSuccess(id: String, wasChecked: Boolean) {
        val newList = data.value
        newList?.find { it.eventId == id }?.status?.ready = wasChecked
        data.postValue(newList)
        //temporary
//        delay(500) {
//            val tempPage = pager.currentItem
//            pager.adapter = pager.adapter
////            ((pager.adapter as CoreValuesPagerAdapter).getItem(if (wasChecked) 1 else 0) as DiscoverActiveFragment).adapter =
////                    ((pager.adapter as CoreValuesPagerAdapter).getItem(if (wasChecked) 0 else 1) as DiscoverActiveFragment).adapter
////            ((pager.adapter as CoreValuesPagerAdapter).getItem(if (wasChecked) 0 else 1) as DiscoverActiveFragment).adapter.notifyDataSetChanged()
//            pager.currentItem = tempPage
//        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        fillData(view)
    }

    private fun fillData(view: View) {
        val layoutManager = object : LinearLayoutManager(view.context, LinearLayoutManager.VERTICAL, false) {
            override fun supportsPredictiveItemAnimations(): Boolean {
                return true
            }
        }
        setupNoItemsTextView()
        adapter = DiscoverActiveAdapter(presenter, data.value?.filter {
            it.status?.ready == showReady
        })
        discoverActiveRecyclerView.adapter = adapter
        discoverActiveRecyclerView.layoutManager = layoutManager
        if (showReady) {
            val animator = SlideInLeftAnimator()
            animator.setInterpolator(AccelerateDecelerateInterpolator())
            discoverActiveRecyclerView.itemAnimator = animator
        } else {
            val animator = SlideInRightAnimator()
            animator.setInterpolator(AccelerateDecelerateInterpolator())
            discoverActiveRecyclerView.itemAnimator = SlideInRightAnimator()
        }
        discoverActiveRecyclerView.itemAnimator.removeDuration = 200
        data.observe(this, Observer { t ->
            checkForEmptyList()
            adapter.setData(t?.filter {
                it.status?.ready == showReady
            })
        })
    }



    private fun setupNoItemsTextView() {
        // TODO: Change this not to depend on showReady flag
        if (showReady) {
            no_items_text_view.setText(R.string.active_empty_page)
        } else {
            no_items_text_view.setText(R.string.discover_empty_page)
        }
    }

    private fun checkForEmptyList() {
        no_items_text_view.animate().alpha(
                data.value?.filter { it.status?.ready == showReady }.let {
                    if (it?.isEmpty() == true) {
                        1.0f
                    } else {
                        0.0f
                    }
                }
        )
//        if (data.value?.filter {
//            it.status?.ready == showReady
//        }?.size == 0 || data.value?.filter {
//            it.status?.ready == showReady
//        } == null) {
//            noItemsText.animate().alpha(1.0f)
//        } else {
//            noItemsText.animate().alpha(0.0f)
//        }
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }


//    companion object {
//        fun newInstance(): DiscoverActiveFragment {
//            // TODO: Add setting the list with the items
//            val discoverActiveFragment = DiscoverActiveFragment()
//            return discoverActiveFragment
//        }
//    }
}