package bg.devlabs.sparrow.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.WelcomeFragment
import bg.devlabs.sparrow.ui.base.InjectionBaseFragment
import bg.devlabs.sparrow.ui.causes.CausesFragment
import bg.devlabs.sparrow.ui.connectedaccounts.ConnectedAccountsFragment
import bg.devlabs.sparrow.ui.corevalues.CoreValuesFragment
import bg.devlabs.sparrow.ui.funding.FundingAccountFragment
import bg.devlabs.sparrow.ui.home.HomeActivity
import bg.devlabs.sparrow.utils.ContextAction
import bg.devlabs.sparrow.utils.onClick
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_settings.*
import org.jetbrains.anko.alert
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SettingsFragment : InjectionBaseFragment(), SettingsContract.View {

    @Inject
    lateinit var presenter: SettingsContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as HomeActivity).hideToolbarButton()
        activity?.setActionBarTitle(getString(R.string.title_settings))

        funding_account.onClick {
            showFragment(FundingAccountFragment(), R.string.funding_fragment)
        }

        connected_accounts.onClick {
            showFragment(ConnectedAccountsFragment(), R.string.connected_accounts_fragment)
        }

        portfolio.onClick {
            showFragment(CausesFragment().apply { this.contextAction = ContextAction.SETTINGS }, R.string.causes_fragment_settings)
        }

        core_values.onClick {
            showFragment(CoreValuesFragment().apply { this.contextAction = ContextAction.SETTINGS }, R.string.core_values_fragment)
        }

        legal.onClick {
            //Legal webview

        }

        logout.onClick {
            activity?.alert(getString(R.string.text_logout_confirmation), getString(R.string.text_logout)) {
                positiveButton("log out") {
                    presenter.onLogoutClick()
                }
//                yesButton {
//                    presenter.onLogoutClick()
//                }
//                noButton {}
                negativeButton("cancel") {}
            }?.show()

        }

        delete.onClick {
            activity?.alert(getString(R.string.text_pause_and_delete_confirmation), getString(R.string.text_pause_and_delete)) {
                //                yesButton {
//                    presenter.onDeleteClick()
//                }
//                noButton {}
                positiveButton("delete") {
                    presenter.onDeleteClick()
                }
//                yesButton {
//                    presenter.onLogoutClick()
//                }
//                noButton {}
                negativeButton("cancel") {}
            }?.show()
        }

    }

    override fun onLogoutSuccess() {
        showToast("Success")
        val fm = activity?.supportFragmentManager
        for (i in 0 until (fm?.backStackEntryCount ?: 0)) {
            fm?.popBackStack()
        }
        showFragment(WelcomeFragment(), R.string.welcome_fragment)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }

}