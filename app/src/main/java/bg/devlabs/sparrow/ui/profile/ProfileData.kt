package bg.devlabs.sparrow.ui.profile


/**
 * Created by Radoslav Yankov on 12.02.2018
 * Dev Labs
 * radoslavyankov@gmail.com
 */
data class ProfileData(val name: String, val url: String)