package bg.devlabs.sparrow.ui.signup

import bg.devlabs.sparrow.data.network.model.CredentialsResponse
import bg.devlabs.sparrow.ui.base.BasePresenter
import retrofit2.adapter.rxjava2.Result
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 01.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SignUpPresenter @Inject constructor(view: SignUpContract.View) :
        BasePresenter<SignUpContract.View>(view), SignUpContract.Presenter {

    override fun onSignUpButtonClicked(email: String, password: String, name: String) {
        compositeDisposable.add(dataManager.createUser(email, password)
                .subscribe({ response: Result<CredentialsResponse> ->
                    if (response.response()?.isSuccessful == true) {
                        dataManager.userId = response.response()?.body()?.user?.userId ?: ""
                        dataManager.xAuth = response.response()?.headers()?.get("auth-user") ?: ""
                        view?.onSignUpSuccess()
//                    dataManager.updateUser(response.response()?.body()?.user?.userId
//                            ?: "", response.response()?.headers()?.get("auth-user") ?: "", name)
//                            .subscribe({ success: Response<Unit> ->
//                                if (response.response()?.code().toString().startsWith("2")) {
//
//                                    view?.onSignUpSuccess()
//
//                                } else {
//                                    view?.onSignUpFail()
//                                }
//                            }, { view?.onSignUpFail() })
                    } else {
                        view?.onSignUpFail()
                    }
                }, { view?.onSignUpFail() })
        )
    }

}