package bg.devlabs.sparrow.ui.welcome

import android.content.Intent
import android.util.Log
import bg.devlabs.sparrow.ui.base.BasePresenter
import bg.devlabs.sparrow.utils.RC_SIGN_IN
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 20.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class WelcomePresenter @Inject constructor(view: WelcomeContract.View)
    : BasePresenter<WelcomeContract.View>(view), WelcomeContract.Presenter {

    override fun onGoogleSignInButtonClicked() {
        val signInIntent = dataManager.googleSignInClient.signInIntent
        view?.startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) { // Google sign in
            compositeDisposable.add(dataManager.onGoogleSignInResult(data)
                    .subscribe({
                        // TODO: Implement - User sign in with Google success
                    }, {
                        // TODO: Implement - User sign in with Google failure
                    })
            )
        } else {// Facebook sign in
            dataManager.onFacebookSignInResult(requestCode, resultCode, data)
        }
    }

    override fun onFacebookSignInButtonClicked() {
        dataManager.registerFacebookLoginCallback()
                ?.subscribe({
                    Log.d(WelcomePresenter::class.java.simpleName, "onFacebookSignInButtonClicked: ")
                }, {
                    view?.showToast(it.localizedMessage)
                })
    }
}