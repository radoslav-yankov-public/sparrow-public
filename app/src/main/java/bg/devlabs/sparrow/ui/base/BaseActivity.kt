package bg.devlabs.sparrow.ui.base

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast

import bg.devlabs.sparrow.R


/**
 * Created by Radoslav Yankov on 31.1.18.
 */

@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity(), BaseContract.View {
    var backPressedEnabled: Boolean = true

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                (supportActionBar?.customView?.findViewById(R.id.title) as TextView).alpha = 0f
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    var isNetworkConnected: Boolean = false
        get() {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED || connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).state == NetworkInfo.State.CONNECTED
        }


    private var dialog: ProgressDialog? = null

    protected fun setupActionBar() {1
        val mToolbar = findViewById<View>(R.id.toolbar) as Toolbar
        mToolbar.setTitleTextColor(resources.getColor(R.color.colorTitle))
        setSupportActionBar(mToolbar)
        setCustomActionBar()
    }

    private fun setCustomActionBar() {
    }

    fun setToolbarElevation(elevation: Float) {
        if (supportActionBar != null) {
            supportActionBar?.elevation = elevation
        }
    }

    override fun shareText(text: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, text)
        startActivity(intent)
    }


    override fun showProgressDialog() {
        dialog = ProgressDialog.show(this, "",
                "Loading. Please wait...", true)
    }

    override fun hideProgressDialog() {
        if (dialog != null) {
            if (dialog?.isShowing == true)
                dialog?.hide()
        }
    }

    override fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    override fun showToast(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_SHORT).show()
    }

    override fun showErrorToast() {
        Toast.makeText(this, getString(R.string.error_generic), Toast.LENGTH_SHORT).show()
    }

    override fun showNetworkError() {
        hideProgressDialog()
//        Log.d(TAG, "showNetworkError: 2")
        //todo check this one
        finish()
    }

    override fun openActivity(activityClass: Class<*>, names: Array<String>, values: Array<String>) {
        val intent = Intent(this, activityClass)
        val size = names.size
        val size2 = values.size
        if (size != size2) {
//            Log.d(TAG, "openActivity: size!=size2")
            return
        }
        for (i in 0 until size) {
            intent.putExtra(names[i], values[i])
        }
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        hideProgressDialog()
    }

    companion object {
        private val TAG = BaseActivity::class.java.simpleName
    }
}

