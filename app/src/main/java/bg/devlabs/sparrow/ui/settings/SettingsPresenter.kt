package bg.devlabs.sparrow.ui.settings

import bg.devlabs.sparrow.ui.base.BasePresenter
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class SettingsPresenter
@Inject constructor(view: SettingsContract.View) : BasePresenter<SettingsContract.View>(view),
        SettingsContract.Presenter {

    override fun onLogoutClick() {
        compositeDisposable.add(dataManager.logout(dataManager.xAuth)
                .subscribe({
                    if (it.response()?.isSuccessful == true) {
                        dataManager.xAuth = ""
                        dataManager.userId = ""
                        view?.onLogoutSuccess()
                    } else {
                        view?.showErrorToast()
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }

    override fun onDeleteClick() {
        compositeDisposable.add(dataManager.delete(dataManager.userId, dataManager.xAuth)
                .subscribe({
                    if (it.response()?.isSuccessful == true) {
                        dataManager.xAuth = ""
                        dataManager.userId = ""
                        view?.onLogoutSuccess()
                    } else {
                        view?.showErrorToast()
                    }
                }, {
                    view?.showErrorToast()
                })
        )
    }
}
