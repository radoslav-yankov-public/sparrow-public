package bg.devlabs.sparrow.ui.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import bg.devlabs.sparrow.R
import bg.devlabs.sparrow.ui.KeyboardHideFragment
import bg.devlabs.sparrow.ui.home.HomeActivity
import bg.devlabs.sparrow.ui.home.ToolbarButtonType
import bg.devlabs.sparrow.utils.load
import bg.devlabs.sparrow.utils.setActionBarTitle
import kotlinx.android.synthetic.main.fragment_profile.*
import javax.inject.Inject


/**
 * Created by Slavi Petrov on 06.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class ProfileFragment : KeyboardHideFragment(), ProfileContract.View {

    @Inject
    lateinit var presenter: ProfileContract.Presenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        presenter.onViewBind()
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.onViewBind()
        (activity as HomeActivity).showToolbarButton(ToolbarButtonType.SETTINGS)
        activity?.setActionBarTitle(getString(R.string.title_profile))

//        editButton.onClick {
//            if (nameTextView.hasFocus()) {
//                presenter.onNameClick(nameTextView.text.toString())
//            } else {
//                nameTextView.focus()
//            }
//        }
    }

    override fun onKeyboardHidden() {
        nameTextView.isFocusable = false
        nameTextView.clearFocus()
    }

    override fun onDataLoaded(data: ProfileData) {
        nameTextView.setText(data.name, TextView.BufferType.EDITABLE)
        loadProfileImage(data.url)
    }

    private fun loadProfileImage(url: String) {
        profileImageView.load(R.drawable.profile)
    }

    override fun onDetach() {
        super.onDetach()
        presenter.onDetach()
    }
}