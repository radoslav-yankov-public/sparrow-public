package bg.devlabs.sparrow.ui.main

import android.support.v4.view.ViewPager
import android.view.View


/**
 * Created by Slavi Petrov on 09.02.2018
 * Dev Labs
 * slavi@devlabs.bg
 */
class FadePageTransformer : ViewPager.PageTransformer {
    override fun transformPage(view: View, position: Float) {
        view.translationX = view.width * -position

        if (position <= -1.0F || position >= 1.0F) {
            view.alpha = 0.0F
        } else if (position == 0.0F) {
            view.alpha = 1.0F
        } else {
            // position is between -1.0F & 0.0F OR 0.0F & 1.0F
            view.alpha = 1.0F - Math.abs(position)
        }
    }
}